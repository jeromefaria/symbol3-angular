/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
.service('ChartService', function Chart() {

  var tables = {
    teams:[
    {name:'HW Team'},
    {name:'SW Team'},
    {name:'Research'},
    {name:'Support'}
    ],
    users:[
    {name:'Tanya Frazier', team:0},
    {name:'Cantrell Briggs', team:0},
    {name:'Cantu Hester', team:0},
    {name:'Ebony Long', team:1},
    {name:'Alisa Simmons', team:1},
    {name:'Medina Carpenter', team:1},
    {name:'Kaufman Dodson', team:2},
    {name:'Cook Garner', team:2},
    {name:'Pugh Warner', team:3},
    {name:'Tabatha Bush', team:3}
    ],
    customers:[
    {name:'BMW'},
    {name:'VW'},
    {name:'Daimler'},
    {name:'Volvo'},
    {name:'Tata'}
    ],
    projects:[
    {name:'LED Lights', customer:0},
    {name:'ECU DEV01', customer:1},
    {name:'Brake Assist', customer:2},
    {name:'Volvo D1 Proto', customer:3},
    {name:'LaserLight', customer:4},
    {name:'iOn Battery', customer:0},
    {name:'Steering System', customer:1},
    {name:'F1 Engine', customer:2},
    {name:'Self Driving', customer:3}
    ],
    tickets:[
    {name:'LED0011', project:0},
    {name:'LED0012', project:0},
    {name:'DEV001 011', project:1},
    {name:'DEV001 012', project:1},
    {name:'BA00 001', project:2},
    {name:'BA00 002', project:2},
    {name:'D1', project:3},
    {name:'LL124', project:4},
    {name:'LL111', project:4},
    {name:'iOn0011', project:5},
    {name:'iOn0012', project:5},
    {name:'SS001 011', project:6},
    {name:'SS001 012', project:6},
    {name:'F100 001', project:7},
    {name:'F100 002', project:7},
    {name:'Self Dev01', project:8}
    ],
    devphases:[
    {name:'Requirement'},
    {name:'Development'},
    {name:'Integration'},
    {name:'Testing'}
    ],
    teams_projects:[
    {team:0, project:1},
    {team:1, project:0},
    {team:2, project:3},
    {team:3, project:2},
    {team:3, project:4},
    {team:1, project:1},
    {team:0, project:5},
    {team:1, project:7},
    {team:2, project:6},
    {team:3, project:3},
    {team:3, project:8}
    ]
  };

  var generatedTimeSpent = [];
  var generatedWorkingTime = [];
  var generatedTimesheetStatus = [];

  var data = {
    teams: {
      key: 'teams',
      label : 'per Team',
      url: '#/charts/teams',
      joinOptions: ['projects', 'devphases', 'customers'],
      unit: 'users'
    },
    users: {
      key: 'users',
      label : 'per User',
      url: '#/charts/users',
      joinOptions: ['projects', 'devphases', 'customers']
    },
    projects: {
      key: 'projects',
      label : 'per Project',
      url: '#/charts/projects',
      array: [ 'LED Lights', 'ECU DEV001', 'Brake Assist', 'Audi S1 Proto', 'LaserLight'],
      joinOptions: ['teams', 'devphases'],
      unit: 'tickets'
    },
    devphases: {
      key: 'devphases',
      label: 'per Development Phase',
      url: '#/charts/devphases',
      joinOptions: ['projects', 'teams', 'customers']
    },
    tickets: {
      key: 'tickets',
      label: 'per Ticket',
      url: '#/charts/tickets',
      joinOptions: ['team', 'devphases']
    },
    customers: {
      key: 'customers',
      label: 'per Customer',
      url: '#/charts/customers',
      joinOptions: ['projects', 'devphases', 'teams']
    }
  };

  function random(min, max){
    return Math.floor(Math.random()*(max-min+1)+min);
  }

  function getNumberUserForTeam(teamName){
    var number = 0;
    var teamId;
    for(var i = 0; i < tables.teams.length; i++){
      if(tables.teams[i].name === teamName){
        teamId = i;
      }
    }
    for(var i = 0; i < tables.users.length; i++){
      if(tables.users[i].team === teamId){
        number++;
      }
    }
    return number;
  }

  function getTotalHoursWorkingTimePerTeam(teamName){
    var hours = 0;
    var teamId;
    for(var i = 0; i < tables.teams.length; i++){
      if(tables.teams[i].name === teamName){
        teamId = i;
      }
    }

    for(var i = 0; i < generatedWorkingTime.length; i++){
      if(generatedWorkingTime[i].teams === teamId){
        hours += generatedWorkingTime[i].work;
        hours += generatedWorkingTime[i].sick;
        hours += generatedWorkingTime[i].holiday;
        hours += generatedWorkingTime[i].overtime;
      }
    }

    return hours;
  }

  function generateDevPhase(teamIndex, userIndex, projectIndex, ticketIndex){
    for(var i = 0; i < tables.devphases.length; i++){
      generatedTimeSpent.push({
        teams:teamIndex,
        users:userIndex,
        customers:tables.projects[projectIndex].customer,
        projects:projectIndex,
        tickets:ticketIndex,
        devphases: i,
        hours:random(7, 9)
      });
    }
  }

  function generateTicket(teamIndex, userIndex, projectIndex){
    for(var i = 0; i < tables.tickets.length; i++){
      if(tables.tickets[i].project === projectIndex){
        generateDevPhase(teamIndex, userIndex, projectIndex, i);
      }
    }
  }

  function generateProject(teamIndex, userIndex){
    for(var i = 0; i < tables.teams_projects.length; i++){
      if(tables.teams_projects[i].team === teamIndex){
        generateTicket(teamIndex, userIndex, tables.teams_projects[i].project);
      }
    }
  }

  function generateUserForTeam(teamIndex){
    for(var i = 0; i < tables.users.length; i++){
      if(tables.users[i].team === teamIndex){
        generateProject(teamIndex, i);
      }
    }
  }

  function generateWorkingTimeForUser(teamIdx) {
    for(var i = 0; i < tables.users.length; i++){
      if(tables.users[i].team === teamIdx){
        generatedWorkingTime.push({
          teams:teamIdx,
          users: i,
          work:random(140,150),
          sick:random(0,8),
          holiday:random(8,16),
          overtime:random(10,20)
        });
      }
    }
  }

  function generateTimesheetStatusForUser(teamIdx){
    var status = ['Filled', 'Not filled'];
    for(var i = 0; i < tables.users.length; i++){
      if(tables.users[i].team === teamIdx){
        generatedTimesheetStatus.push({
          teams:teamIdx,
          users: i,
          status: status[random(0,1)]
        });
      }
    }
  }

  this.generateData = function(){
    generatedTimeSpent = [];
    generatedWorkingTime = [];
    generatedTimesheetStatus = [];


    for(var i = 0; i < tables.teams.length; i++){
      generateUserForTeam(i);
      generateWorkingTimeForUser(i);
      generateTimesheetStatusForUser(i);
    }
  };

  function extractTotals(objName, objId){
    //d3 acceptable format: one entry in an array per series
    var serie = [{key:'', values: []}];
    var map = {};

    //Extract data from generated sources for the given objName
    for(var i = 0; i < generatedTimeSpent.length; i++){
      var idx = generatedTimeSpent[i][objName];
      var key = tables[objName][idx].name;
      if(objId === undefined || objId === key){
        if(map[key] === undefined){
          map[key] = 0;
        }
        map[key] += generatedTimeSpent[i].hours;
      }
    }

    //Parse them to a 2 index's array format
    for(var i = 0; i < Object.keys(map).length; i++){
      var idx = Object.keys(map)[i];
      serie[0].values.push([idx, map[idx]]);
    }
    return serie;

  }

  function extractTotalsRelation(obj1, obj2, objId){
    //d3 acceptable format: one entry in an array per series

    var map = {};

    //Extract data from generated sources for the given objName
    for(var i = 0; i < generatedTimeSpent.length; i++){
      var idx1 = generatedTimeSpent[i][obj2];
      var key1 = tables[obj2][idx1].name;

      var idx2 = generatedTimeSpent[i][obj1];
      var key2 = tables[obj1][idx2].name;

      //if the id is not specified, the extraction will be made to all present
      //records of type obj1
      if(objId === undefined || objId === key1){
        if(map[key2] === undefined){
          map[key2] = {};
        }
        if(map[key2][key1] === undefined){
          map[key2][key1] = 0;
        }
        map[key2][key1] += generatedTimeSpent[i].hours;
      }
    }

    var chart = [];
    for(var i = 0; i < Object.keys(map).length; i++){
      var idx1 = Object.keys(map)[i];
      var serie = {key: idx1, values: []};
      for(var j = 0; j < Object.keys(map[idx1]).length; j++){
        var idx2 = Object.keys(map[idx1])[j];
        serie.values.push([idx2, map[idx1][idx2]]);
      }
      chart.push(serie);
    }

    return chart;
  }

  function extractOverview(objName){
    var overview = {};
    overview.id = data[objName].key;
    overview.title = '';
    overview.url = data[objName].url;
    overview.titleLink = data[objName].label;

    var chart = {key:'totals', label:'totals', data: extractTotals(objName)};
    overview.chart = chart;

    // for(var i = 0; i < data[objName].joinOptions.length; i++){
    //   var option = data[objName].joinOptions[i];
    //   var dataObj = data[option];
    //   chart = {key:dataObj.key, label:dataObj.label, data:extractTotalsRelation(objName, option)};
    //   overview.charts.push(chart);
    // }

    return overview;
  }

  function extractWorkingTimeOverview(objName, objId){
    var overview = {};
    overview.id = 'workingtime-' + data[objName].key;
    overview.title = '';
    overview.url = data[objName].url;
    overview.titleLink = data[objName].label;
    overview.chart = {key: overview.id, data: []};
    overview.data = [];

    var items = ['work', 'sick', 'holiday', 'overtime'];

    //d3 acceptable format: one entry in an array per series
    var map = {};

    //Extract data from generated sources for the given objName
    for(var i = 0; i < generatedWorkingTime.length; i++){
      var idx = generatedWorkingTime[i][objName];
      var key = tables[objName][idx].name;
      //if .work is undefined, all need to be initialized
      if(map.work === undefined){
        map.work = {};
        map.sick = {};
        map.holiday = {};
        map.overtime = {};
      }

      if(map.work[key] === undefined){
        map.work[key] = 0;
      }
      if(map.sick[key] === undefined){
        map.sick[key] = 0;
      }
      if(map.holiday[key] === undefined){
        map.holiday[key] = 0;
      }
      if(map.overtime[key] === undefined){
        map.overtime[key] = 0;
      }
      map.work[key] += generatedWorkingTime[i].work;
      map.sick[key] += generatedWorkingTime[i].sick;
      map.holiday[key] += generatedWorkingTime[i].holiday;
      map.overtime[key] += generatedWorkingTime[i].overtime;

    }

    //Parse them to a 2 index's array format
    for(var i = 0; i < Object.keys(map).length; i++){
      var idx = Object.keys(map)[i];
      var serie = {key: idx, values: []};
      for(var j = 0; j < Object.keys(map[idx]).length; j++){
        var idx2 = Object.keys(map[idx])[j];
        serie.values.push([idx2, map[idx][idx2]/getTotalHoursWorkingTimePerTeam(idx2)*200]);
      }
      overview.chart.data.push(serie);
      overview.data.push(serie);
    }

    return overview;
  }

  function extractTimesheetStatusOverview(objName, objId){
    var overview = {};
    overview.id = 'timesheetstatus-' + data[objName].key;
    overview.title = '';
    overview.url = data[objName].url;
    overview.titleLink = data[objName].label;
    overview.chart = {key: overview.id, data: []};
    overview.data = [];

    //d3 acceptable format: one entry in an array per series
    var map = {};

    //Extract data from generated sources for the given objName
    for(var i = 0; i < generatedTimesheetStatus.length; i++){
      var idx = generatedTimesheetStatus[i][objName];
      var key = tables[objName][idx].name;
      //if .work is undefined, all need to be initialized 
      if(map['Filled'] === undefined){
        map['Filled'] = {};
        map['Not filled'] = {};
      }

      if(map[generatedTimesheetStatus[i].status][key] === undefined){
        map['Filled'][key] = 0;
        map['Not filled'][key] = 0;
      }

      map[generatedTimesheetStatus[i].status][key] += 1;
    }

    //Parse them to a 2 index's array format
    for(var i = 0; i < Object.keys(map).length; i++){
      var idx = Object.keys(map)[i];
      var serie = {key: idx, values: []};
      for(var j = 0; j < Object.keys(map[idx]).length; j++){
        var idx2 = Object.keys(map[idx])[j];
        serie.values.push([idx2, map[idx][idx2]/getNumberUserForTeam(idx2)]);
      }
      overview.chart.data.push(serie);
      overview.data.push(serie);
    }

    return overview;
  }

  this.getOverviewData = function(){
    var charts = [];
    //Time Spent
    charts.push(extractOverview('teams'));
    charts.push(extractOverview('customers'));
    charts.push(extractOverview('projects'));
    charts.push(extractOverview('devphases'));

    return charts;
  };

  this.getWorkingTimeOverviewData = function(){
    return extractWorkingTimeOverview('teams');
  };

  this.getTimesheetStatusOverviewData = function(){
    return extractTimesheetStatusOverview('teams');
  };

  this.getOverviewDataForObject = function(setName, objId){
    if(setName === 'workingtime-teams'){
      return extractWorkingTimeOverview('teams', objId);
    }
    if(setName === 'timesheetstatus-teams'){
      return extractTimesheetStatusOverview('teams', objId);
    }
    var overview = {};
    overview.id = data[setName].key;
    overview.title = data[setName].label;
    overview.data = extractTotals(setName, objId);
    return overview;
  };

  this.getOverviewDataForUnit = function(setName, objId){
    var overview = {};

    var dataObj = data[setName];

    if(dataObj.unit === undefined){
      return;
    }

    overview.id = data[dataObj.unit].key;
    overview.title = data[dataObj.unit].label;
    overview.data = extractTotalsRelation(dataObj.unit, setName, objId);
    return overview;
  };

  this.getTimesheetStatusForUnit = function(setName, objId){
    var overview = {};
    var dataObj = data[setName];
    overview.id = 'timesheetstatus-' + data[dataObj.unit].key;
    overview.title = data[dataObj.unit].label;
    overview.chart = {key: overview.id, data: []};
    overview.data = [];

    //d3 acceptable format: one entry in an array per series
    var map = {};

    //Extract data from generated sources for the given objName
    for(var i = 0; i < generatedTimesheetStatus.length; i++){
      var idx1 = generatedTimesheetStatus[i][dataObj.unit];
      var idx2 = generatedTimesheetStatus[i][dataObj.key];

      var key1 = tables[dataObj.unit][idx1].name;
      var keyParent = tables[dataObj.key][idx2].name;

      if(keyParent === objId){
        //if .work is undefined, all need to be initialized 
        if(map['Filled'] === undefined){
          map['Filled'] = {};
          map['Not filled'] = {};
        }

        if(map[generatedTimesheetStatus[i].status][key1] === undefined){
          map['Filled'][key1] = 0;
          map['Not filled'][key1] = 0;
        }

        map[generatedTimesheetStatus[i].status][key1] += 1;
      }
    }

    //Parse them to a 2 index's array format
    for(var i = 0; i < Object.keys(map).length; i++){
      var idx = Object.keys(map)[i];
      var serie = {key: idx, values: []};
      for(var j = 0; j < Object.keys(map[idx]).length; j++){
        var idx2 = Object.keys(map[idx])[j];
        serie.values.push([idx2, map[idx][idx2]]);
      }
      overview.chart.data.push(serie);
      overview.data.push(serie);
    }

    return overview;
  };

  this.getWorkingTimeForUnit = function(setName, objId){
    var overview = {};
    var dataObj = data[setName];
    overview.id = 'workingtime-' + data[dataObj.unit].key;
    overview.title = data[dataObj.unit].label;
    overview.chart = {key: overview.id, data: []};
    overview.data = [];

    var items = ['work', 'sick', 'holiday', 'overtime'];

    //d3 acceptable format: one entry in an array per series
    var map = {};

    //Extract data from generated sources for the given objName
    for(var i = 0; i < generatedWorkingTime.length; i++){
      var idx1 = generatedWorkingTime[i][dataObj.unit];
      var idx2 = generatedWorkingTime[i][dataObj.key];
      var key1 = tables[dataObj.unit][idx1].name;
      var keyParent = tables[dataObj.key][idx2].name;

      if(keyParent === objId){
        //if .work is undefined, all need to be initialized
        if(map.work === undefined){
          map.work = {};
          map.sick = {};
          map.holiday = {};
          map.overtime = {};
        }

        if(map.work[key1] === undefined){
          map.work[key1] = 0;
        }
        if(map.sick[key1] === undefined){
          map.sick[key1] = 0;
        }
        if(map.holiday[key1] === undefined){
          map.holiday[key1] = 0;
        }
        if(map.overtime[key1] === undefined){
          map.overtime[key1] = 0;
        }
        map.work[key1] += generatedWorkingTime[i].work;
        map.sick[key1] += generatedWorkingTime[i].sick;
        map.holiday[key1] += generatedWorkingTime[i].holiday;
        map.overtime[key1] += generatedWorkingTime[i].overtime;
      }
    }

    //Parse them to a 2 index's array format
    for(var i = 0; i < Object.keys(map).length; i++){
      var idx = Object.keys(map)[i];
      var serie = {key: idx, values: []};
      for(var j = 0; j < Object.keys(map[idx]).length; j++){
        var idx2 = Object.keys(map[idx])[j];
        serie.values.push([idx2, map[idx][idx2]]);
      }
      overview.chart.data.push(serie);
      overview.data.push(serie);
    }

    return overview;
  };

  this.getOverviewDataForJoinOptions = function(setName, destination, objId){
    for(var i = 0; i < data[setName].joinOptions.length; i++){
      var overview = {};
      var option = data[data[setName].joinOptions[i]];
      overview.id = option.key;
      
      var titleStr = option.label;
      overview.title = titleStr;

      overview.data = extractTotalsRelation(data[setName].joinOptions[i], setName, objId);
      destination.push(overview);
    }
  };
});
