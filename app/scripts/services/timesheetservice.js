/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
.service('TimesheetService', ['$localStorage', 'TaskService', function($localStorage, TaskService) {

	if($localStorage.records === undefined){
		$localStorage.records = {};
	}

	this.workEvents = {
		textColor: 'white',
		events: []
	}

	this.leaveEvents = {
		textColor: 'white',
		events: []
	}

	this.newEvents = {
		textColor: 'white',
		events: []
	}

	this.eventSource = [this.workEvents, this.leaveEvents, this.newEvents];

	this.getLeaveTypes = function(){
		var types = {
			overtime:'Overtime',
			vacation:'Vacation',
			sickness:'Sickness'
		};
		return types;
	};

	this.saveWorkRecord = function(record){
		if($localStorage.records['work'] === undefined){
			$localStorage.records['work'] = [];
		}

		$localStorage.records['work'].push({
			task: record.task,
			start: record.start,
			end: record.end,
			pause: record.pause
		});

		this.getEvents();
	};

	this.saveLeaveRecord = function(record){
		if($localStorage.records['leave'] === undefined){
			$localStorage.records['leave'] = [];
		}

		$localStorage.records['leave'].push({
			type: record.type,
			start: record.start,
			end: record.end
		});


		this.getEvents();
	};

	this.saveNewRecord = function(record){
		if($localStorage.records['new'] === undefined){
			$localStorage.records['new'] = [];
		}

		$localStorage.records['new'].push({
			entryType: record.entryType,
			fields: record.fields,
			start: record.start,
			end: record.end,
			pause: record.pause
		});

		this.getEvents();
	};

	this.getEvents = function(){
		if($localStorage.records['work'] !== undefined && $localStorage.records['work'].length !== 0){
			var records = $localStorage.records['work'];
			this.workEvents.events = [];
			for(var i = 0; i < records.length; i++){
				var record = {
					type: 'work',
					title: TaskService.getTitleString(records[i].task),
					start: records[i].start,
					end: records[i].end,
					allDay: false
				};
				this.workEvents.events.push(record);
			}
		}

		if($localStorage.records['leave'] !== undefined && $localStorage.records['leave'].length !== 0){
			var records = $localStorage.records['leave'];
			this.leaveEvents.events = [];
			for(var i = 0; i < records.length; i++){
				var startDate = new Date(records[i].start);
				startDate.setHours(7);

				var endDate = new Date(records[i].end);
				endDate.setHours(22);

				var record = {
					type: records[i].type,
					title: this.getLeaveTypes()[records[i].type],
					start: startDate,
					end: endDate,
					allDay: false
				};
				this.leaveEvents.events.push(record);
			}
		}

		if($localStorage.records['new'] !== undefined && $localStorage.records['new'].length !== 0){
			var records = $localStorage.records['new'];
			this.newEvents.events = [];
			for(var i = 0; i < records.length; i++){
				var record = {
					type: 'new',
					title: TaskService.getTitleStringFromFields(records[i].entryType, records[i].fields),
					start: records[i].start,
					end: records[i].end,
					allDay: false
				};
				this.newEvents.events.push(record);
			}
		}
	};

	this.getEvents();
}]);