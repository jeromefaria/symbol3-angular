/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
.service('ToolsService', function Tools() {

	this.toolsItems = [];

	this.setItems = function(items){
		this.toolsItems = items;
	};
});