/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
.service('TaskService', ['$localStorage', function ($localStorage){
	
	this.init = function(){
		if($localStorage.tasks === undefined || Object.keys($localStorage.tasks).length === 0){
			$localStorage.tasks = {
				0 : {
					typeId:0,
					type:'Continental Standard',
					project:'Audi LaserLight 001',
					generic:'Meeting',
					validUntil:'30.04.2014',
					status:'valid'
				},
				1 : {
					typeId:1,
					type:'Continental Generic',
					generic:'Meeting',
					validUntil:'30.04.2014',
					status:'valid'
				},
				2 : {
					typeId:2,
					type:'Continental Ticket',
					project:'Audi LaserLight 001',
					ticket:'audill00121',
					devphase:'Requirement',
					validUntil:'30.04.2014',
					status:'valid'
				}
			};
			$localStorage.currentTask = 3;
		}

		if($localStorage.entryTypes === undefined || $localStorage.entryTypes.length === 0){
			$localStorage.entryTypes = [
				{
					id: 0,
					name: 'Continental Standard',
					fields: [
						{
							id:'project',
							label: 'Project',
							type: 'text',
							placeholder: 'Insert the project name'
						},
						{
							id:'generic',
							label: 'Generic Task',
							type: 'select',
							values: ['Meeting', 'Travel', 'Training']
						}
					]
				},
				{
					id: 1,
					name: 'Continental Generic',
					fields: [
						{
							id:'generic',
							label: 'Generic Task',
							type: 'select',
							values: ['Meeting', 'Travel', 'Training']
						}
					]
				},
				{
					id: 2,
					name: 'Continental Ticket',
					fields: [
						{
							id: 'project',
							label: 'Project',
							type: 'text',
							placeholder: 'Insert the project name'
						},
						{
							id: 'ticket',
							label: 'Ticket',
							type: 'text',
							placeholder: 'Insert your ticket reference'
						},
						{
							id: 'devphase',
							label: 'Development Phase',
							type: 'select',
							values: ['Requirement', 'Development', 'Testing', 'Integration']
						}
					]
				},
				{
					id: 3,
					name: 'Team Management',
					fields: [
						{
							id: 'team',
							label: 'Team Name',
							type: 'text',
							placeholder: 'Insert the name of your team'
						},
						{
							id: 'devphase',
							label: 'Development Phase',
							type: 'select',
							values: ['Requirement', 'Development', 'Testing', 'Integration']
						}
					]
				}
			];
		}
	};

	this.taskToString = function(task){
		switch(task.typeId){
			case 0:
				return task.type + ' - ' + task.project + ' / ' + task.generic;
			case 1:
				return task.type + ' - ' + task.generic;
			case 2:
				return task.type + ' - ' + task.project + ' / ' + task.ticket + ' / ' + task.devphase;
			case 3:
				return task.team + ' - ' + task.devphase;
		}
	};

	this.getTitleString = function(taskId){
		var task = $localStorage.tasks[taskId];
		switch(task.typeId){
			case 0:
				return task.project;
			case 1:
				return task.generic;
			case 2:
				return task.ticket;
			case 3:
				return 'Management - ' + task.team;
		}
	};

	this.getTitleStringFromFields = function(entryType, fields){
		switch(entryType.id){
			case 0:
				return fields.project;
			case 1:
				return fields.generic;
			case 2:
				return fields.ticket;
			case 3:
				return 'Management - ' + fields.team;
		}
	};

	this.getTasks = function(){
		return $localStorage.tasks;
	};

	this.getEntryTypes = function(){
		return $localStorage.entryTypes;
	};

	this.getAndIncrementCurrentTaskIdx = function(){
		var currentIdx = $localStorage.currentTask;
		$localStorage.currentTask++;
		return currentIdx;
	};

	this.saveTask = function(task){
		$localStorage.tasks[$localStorage.currentTask] = task;
		$localStorage.currentTask++;
	};

	this.deleteTask = function(id){
		$localStorage.tasks[id].status = 'deleted';
	};
}]);