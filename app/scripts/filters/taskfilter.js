/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .filter('taskFilter', function () {
    return function(input) {
    	var temp = input;
    	var keys = Object.keys(temp);

    	for(var i = 0; i < keys.length; i++){
    		var key = keys[i];
    		if(temp[key].status === 'deleted'){
    			delete temp[key];
    		}
    	}
    	return temp;
	};
  });
