/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .controller('MenuCtrl', function ($scope) {

    $scope.menuItems = [
      {
        label: 'Dashboard',
        url: '#/dashboard',
        icon: 'glyphicon glyphicon-dashboard'
      },
      {
        label: 'Users',
        url: '#/users',
        icon: 'glyphicon glyphicon-user'
      },
      {
        label: 'Calendar',
        url: '#/calendar',
        icon: 'glyphicon glyphicon-calendar'
      },
      {
        label:'My Tasks',
        url: '#/tasks',
        icon: 'glyphicon glyphicon-tasks'
      }
    ];
  });
