/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .controller('CalendarCtrl', function ($scope, $modal, $filter, ToolsService, TimesheetService) {

    ToolsService.setItems([
      {
        label: 'Add Record',
        onclick: function(){
          $scope.openNewRecordDialog();
        },
        url: 'javascript:',
        icon: 'glyphicon glyphicon-plus'
      },
      {
        label: 'Overview',
        url: 'javascript:',
        icon: 'glyphicon glyphicon-eye-open'
      }
    ]);

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    // $scope.calendarOnDayClick = function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
    //     alert('clicked on ' + event);
    // }

    // $scope.calendarOnEventClick = function(event){
    //     alert(event.start + event.end + event.title);
    // }
    
    $scope.events = TimesheetService.eventSource;

    // $scope.$watch(
    //   function(){
    //     return TimesheetService.getEvents();
    //   },
    //   function( newValue, oldValue ) {
    //     $scope.events = newValue;
    //     //Not working
    //     $scope.myCalendar.fullCalendar('refetchEvents');  
    //   },true
    // );

    $scope.calendarOnSelect = function(start, end){
      // if(this.name === 'agendaWeek'){
        $scope.openNewRecordDialog(new Date(start), new Date(end));  
      // }
    };

    $scope.calendarConfig = {
        editable: true,
        defaultView: 'agendaWeek',
        aspectRatio: 1.5,
        minTime: 7,
        maxTime: 22,
        header:{
            left: 'agendaWeek month',
            center: 'prev,title,next',
            right: ''
        },
        selectable:'true',
        axisFormat: 'HH:mm',
        timeFormat: {
            agenda: 'HH:mm{ - HH:mm}',
            month: 'HH:mm{ - HH:mm}'
        },
        select: $scope.calendarOnSelect
    };

    // var vacations = {
    //    color: '#7f8c8d',
    //    textColor: 'white',
    //    events: [
    //       {type:'vacations',title: 'Vacation',start: new Date(y, m, d),end: new Date(y, m, d+2),allDay: true}
    //     ]
    // };
    // $scope.events.push(vacations);

    // var records = {
    //    color: '#34495e',
    //    textColor: 'white',
    //    events: [
    //       {type:'records',title: 'Audi LaserLight 001021',start: new Date(y, m, 1, 8, 0),end: new Date(y, m, 1, 13,0), allDay: false},
    //       {type:'records',title: 'Audi LaserLight 001021',start: new Date(y, m, 1, 14, 0),end: new Date(y, m, 1, 18,0), allDay: false},
    //       {type:'records',title: 'Audi LaserLight 001021',start: new Date(y, m, 2, 8, 0),end: new Date(y, m, 2, 18,0), allDay: false}
    //     ]
    // };
    // $scope.events.push(records);

    $scope.openNewRecordDialog = function(start, end){
      var modalInstance = $modal.open({
        windowClass: 'modals in',
        templateUrl: 'views/modals/newTimesheetRecord.html',
        controller: 'NewRecordCtrl',
        resolve: {
          start: function () {
            return start;
          },
          end: function() {
            return end;
          }
        } 
      });
    };
  })
  .controller('NewRecordCtrl', function($scope, $modalInstance, $filter, TaskService, TimesheetService, start, end){
    TaskService.init();

    $scope.wizardState = 0;
    $scope.record = {
      clickedStart: start,
      clickedEnd: end
    };  
    $scope.favoriteTasks = [];
    $scope.leaveTypes = TimesheetService.getLeaveTypes();

    var tasks = TaskService.getTasks();
    var keys = Object.keys(tasks);
    for(var i = 0; i < keys.length; i++){
      var key = keys[i];
      var typeId = tasks[key].typeId;
      $scope.favoriteTasks.push([key, TaskService.taskToString(tasks[key])]);
    }

    $scope.selectFavorite = function(key){
      $scope.wizardState = 1;
      $scope.record.type = 'work';
      $scope.record.task = key;

      $scope.task = TaskService.getTasks()[key];
      $scope.record.entryType = TaskService.getEntryTypes()[tasks[key].typeId];

      if($scope.record.clickedStart !== undefined){
        $scope.record.start = $filter('date')($scope.record.clickedStart, 'yyyy-MM-ddTHH:mm');
        $scope.record.end = $filter('date')($scope.record.clickedEnd === undefined ? $scope.record.clickedStart : $scope.record.clickedEnd, 'yyyy-MM-ddTHH:mm');
      }

      $scope.record.pause = '00:00';
    };

    $scope.selectLeave = function(key){
      $scope.record.type = key;
      $scope.wizardState = 2;

      if($scope.record.clickedStart !== undefined){
        $scope.record.start = $filter('date')($scope.record.clickedStart, 'yyyy-MM-dd');
        $scope.record.end = $filter('date')($scope.record.clickedEnd === undefined ? $scope.record.clickedStart : $scope.record.clickedEnd, 'yyyy-MM-dd');
      }
    };

    $scope.insertNew = function(){
      $scope.wizardState = 3;
      $scope.entryTypes = TaskService.getEntryTypes();

      if($scope.record.clickedStart !== undefined){
        $scope.record.start = $filter('date')($scope.record.clickedStart, 'yyyy-MM-ddTHH:mm');
        $scope.record.end = $filter('date')($scope.record.clickedEnd === undefined ? $scope.record.clickedStart : $scope.record.clickedEnd, 'yyyy-MM-ddTHH:mm');
      }

      $scope.record.pause = '00:00';
    };

    $scope.saveWork = function(){
      var record = $scope.record;
      TimesheetService.saveWorkRecord(record);

      $modalInstance.close(); 
    };

    $scope.saveLeave = function(){
      var record = $scope.record;
      TimesheetService.saveLeaveRecord(record);

      $modalInstance.close(); 
    };
    $scope.saveNew = function(){
      var record = $scope.record;
      TimesheetService.saveNewRecord(record);

      $modalInstance.close(); 
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.previous = function(){
      if($scope.wizardState === 4){
        $scope.wizardState = 3;   
      }
      else {
        $scope.wizardState = 0;   
      } 
    };

    $scope.next = function(){
      if($scope.record.entryType === undefined){
        $scope.alert = {
          type: 'danger',
          msg: 'Please select one entry type before continuing.'
        };
      }else{
        $scope.alert = undefined;
        $scope.record.fields = {};
        $scope.wizardState = 4;
      }
    };
  });
