'use strict';

angular.module('symbol3App')
.controller('GraphCtrl', function ($scope) {
  $scope.stats = [
    {
      'team': 'TeamCustomerX',
      'users': [
        {
          'name': 'John Doe',
          'entries': [
            {
              'type': 'Extended',
              'project': 'P112212',
              'ticket': 'P112212.1',
              'package': 'P112212.1.1',
              'developmentPhase': 'Requirement'
            }
          ]
        },
        {
          'name': 'Michelle Obama',
          'entries': [
            {
              'type': 'Extended',
              'project': 'P112212',
              'ticket': 'P112212.2',
              'package': 'P112212.2.2',
              'developmentPhase': 'SW Development'
            }
          ]
        }
      ]
    },
    {
      'team': 'TeamCustomerY',
      'users': [
        {
          'name': 'John Doedoe',
          'entries': [
            {
              'type': 'Extended',
              'project': '819203104',
              'ticket': '819203104.10',
              'package': '819203104.10.1',
              'developmentPhase': 'Integration'
            }
          ]
        },
        {
          'name': 'Michael Knight',
          'entries': [
            {
              'type': 'Standard',
              'project': '819203104',
              'task': 'Training'
            },
            {
              'type': 'Generic',
              'task': 'Meeting'
            }
          ]
        }
      ]
    },
    {
      'team': 'TeamCustomerZ',
      'users': [
        {
          'name': 'Charlie Echo',
          'entries': [
            {
              'type': 'Extended',
              'project': 'E.Car 2014 Concept',
              'ticket': 'E2014.32',
              'package': 'E2014.32.1',
              'developmentPhase': 'Requirement'
            },
            {
              'task': 'Meeting'
            }
          ]
        },
        {
          'name': 'Fausto Coelho',
          'entries': [
            {
              'type': 'Standard',
              'project': 'E.Car 2014 Concept',
              'task': 'Training'
            },
            {
              'project': 'E.Car 2014 Concept',
              'ticket': 'E2014.32',
              'package': 'E2014.32.2',
              'developmentPhase': 'Integration'
            }
          ]
        }
      ]
    }
  ];
/*
  var colors = ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f1c40f', '#e67e22', '#e74c3c', '#ecf0f1', '#95a5a6', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d'];
  var color = [randomColor(), randomColor()];
  var data = [4, 8, 15, 16, 23, 42];
  // var data = [];
  var width = 500;
  var height = 500;
  var offset = 20;
  // var scale = 10;
  var lineData = [];

  for(var i=0; i<20; i++) {
    var x = Math.random() * width;
    var y = Math.random() * width;

    lineData.push({ x: x, y: y});
  }

  function randomColor() {
    var i = parseInt(Math.random() * colors.length);
    return colors[i];
  }

  var widthScale = d3.scale.linear()
      .domain([0, 42])
      .range([0, width]);

  var colorScale = d3.scale.ordinal()
      .domain([0, 42])
      .range([randomColor(), randomColor(), randomColor(), randomColor(), randomColor(), randomColor()]);

  var axis = d3.svg.axis()
      .ticks(10)
      .scale(widthScale);

  var canvas = d3.select('#chart')
      .append('svg')
      .attr('width', width)
      .attr('height', height);

  var group = canvas.append('g')
      .attr('transform', 'translate(200, 200)');

  var line = d3.svg.line()
      .x(function(d) { return d.x; })
      .y(function(d) { return d.y; });

  var arc = d3.svg.arc()
      .innerRadius(150)
      .outerRadius(200);

  var pie = d3.layout.pie()
      .value(function(d) { return d; });

  var arcs = group.selectAll('.arc')
      .data(pie(data))
      .enter()
        .append('g')
        .attr('class', 'arc');

  arcs.append('path')
    .attr('d', arc)
    .attr('fill', function(d) { return colorScale(d.data); });

  arcs.append('text')
    .attr('transform', function(d) { return 'translate('+ arc.centroid(d) + ')'; })
    .attr('text-anchor', 'middle')
    .attr('font-weight', 'bold')
    .attr('fill', '#ffffff')
    .text(function(d) { return d.data; });

  group.selectAll('path')
      .data([lineData])
      .enter()
        .append('path')
        .attr('d', line)
        .attr('fill', 'none')
        .attr('stroke', randomColor())
        .attr('stroke-width', 10);
*/
  /*var canvas = d3.select('#chart')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('transform', 'translate(0, 20)');*/


  /*var circle = canvas.append('circle')
      .attr('cx', 250)
      .attr('cy', 250)
      .attr('r', 50)
      .attr('fill', 'red');

  var rect = canvas.append('rect')
      .attr('width', 200)
      .attr('height', 50)
      .attr('fill', '#00ff00');

  var line = canvas.append('line')
      .attr('x1', 100)
      .attr('y1', 100)
      .attr('x2', 400)
      .attr('y2', 400)
      .attr('stroke', 'blue')
      .attr('stroke-width', 5);*/

  /*var bars = canvas.selectAll('rect')
      .data(data)
      .enter()
        .append('rect')
        .attr('width', 0)
        .attr('opacity', 0)
        .transition()
        .attr('opacity', 1)
        .attr('width', function(d) { return widthScale(d); })
        .attr('height', offset)
        .attr('y', function(d, i) { return i * (offset * 2); })
        .attr('fill', function(d) { return colorScale(d); })
        .attr('rx', 10)
        .attr('ry', 10);

  canvas.append('g')
      .attr('transform', 'translate(0, 250)')
      .attr('fill', randomColor())
      .call(axis);*/

});
