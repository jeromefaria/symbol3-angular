/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .controller('DashboardCtrl', function ($scope, $window, ChartService, ToolsService) {
    ToolsService.setItems([]);

    ChartService.generateData();
    $scope.totals = ChartService.getOverviewData();
    $scope.workingtime = ChartService.getWorkingTimeOverviewData();
    $scope.timesheetstatus = ChartService.getTimesheetStatusOverviewData();

    $scope.$on('elementClick.directive', function(angularEvent, $event){
        //The identifier of this dataset is the id of the direct div.chart parent element
        var dataSetId = angular.element($event.e.target).parents('.chart').prop('id');
        $window.location.href = '#/charts/' + dataSetId + '?name=' + $event.point[0];
    });

    // var colorArray = ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f1c40f', '#e67e22', '#e74c3c', '#ecf0f1', '#95a5a6', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d'];
		var colorArray = ['#f76700', '#f79800', '#f7ca00', '#f1f500', '#96dc00', '#00c614', '#008fc6', '#0049c6', '#2900c6'];

		// var colorArray = ['#f79800', '#f7c900', '#f1f500', '#98dc00', '#00c612', '#008fc6', '#0049c6', '#2900c6', '#9000c6', '#d4006e', '#f70800'];

		var randomColor = parseInt(Math.random() * colorArray.length);
		console.log(randomColor);

    $scope.colorFunction = function() {
        return function(d, i) {
            return colorArray[i];
        };
    };

    $scope.getMargin = function(){
      return {left:100,bottom:20,top:20};
    };

    $scope.yAxisFormatFunction = function(){
        return function(d){
            return d3.format('%')(d);
        }
    };

    // console.log(window.innerWidth);

    // $scope.isMobile = 'undefined';

    // window.onresize = function() {
    //   if(window.innerWidth < 992) {
    //     $scope.isMobile = 'hide';
    //   } else {
    //     $scope.isMobile = false;
    //   }

    //   console.log($scope.isMobile);
    // };

    // $scope.hideElement = function() {
    //   if($scope.isMobile) {
    //     return 'hide';
    //   } else {
    //     return false;
    //   }
    // };

  });
