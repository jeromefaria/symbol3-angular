/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .controller('ChartCtrl', function ($scope, $location, $window, $routeParams, ChartService, ToolsService){

    ToolsService.setItems([]);

    ChartService.generateData();
    $scope.dataSet = $routeParams.setName;

    $scope.$on('elementClick.directive', function(angularEvent, $event){
      //The identifier of this dataset is the id of the direct div.chart parent element
      
      if(angular.element($event.e.target).parents('div.pane-left').length > 0){
        $scope.dataId = $event.point[0];
        $window.history.pushState('', '', '#/charts/' + $scope.dataSet + '?name=' + $event.point[0]);

        //Add selection class and style to bar
        // $event.e.target.addClass('selected');

        $scope.updateCharts();

        $scope.$digest();  
      }
    });

    // var colorArray = ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f1c40f', '#e67e22', '#e74c3c', '#ecf0f1', '#95a5a6', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d'];
    var colorArray = ['#f76700', '#f79800', '#f7ca00', '#f1f500', '#96dc00', '#00c614', '#008fc6', '#0049c6', '#2900c6'];


//		var colorArray = ['#f79800', '#f7c900', '#f1f500', '#98dc00', '#00c612', '#008fc6', '#0049c6', '#2900c6', '#9000c6', '#d4006e', '#f70800'];


    $scope.colorFunction = function() {
      return function(d, i) {
        return colorArray[i];
      };
    };

    $scope.getHeight = function(chart){
      if(chart === undefined){
        return 50;
      }
      return 50 + 60 * chart.data[0].values.length;
    };

    $scope.yAxisTimesheetFormatFunction = function(){
        return function(d){
            return d3.format('%')(d);
        }
    };

    $scope.overview = ChartService.getOverviewDataForObject($scope.dataSet);
    $scope.dataId = $routeParams.name;

    $scope.updateCharts = function(){
      $scope.breakdownCharts = [];

      if($scope.dataId !== undefined && $scope.dataId.length !== 0){
        if($scope.dataSet.indexOf('workingtime-') > -1){
          $scope.breakdownCharts.push(ChartService.getWorkingTimeForUnit($scope.dataSet.replace('workingtime-', ''), $scope.dataId));
        }else if($scope.dataSet.indexOf('timesheetstatus-') > -1){
          $scope.breakdownCharts.push(ChartService.getTimesheetStatusForUnit($scope.dataSet.replace('timesheetstatus-', ''), $scope.dataId));
        }else{
          $scope.breakdownCharts.push(ChartService.getOverviewDataForUnit($scope.dataSet, $scope.dataId));
          ChartService.getOverviewDataForJoinOptions($scope.dataSet, $scope.breakdownCharts, $scope.dataId);  
        }
      }
    };

    $scope.updateCharts();

    
});