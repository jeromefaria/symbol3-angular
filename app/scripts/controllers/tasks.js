/* jshint devel:true */
/* global angular */

'use strict';

var app = angular.module('symbol3App');
app.controller('TasksCtrl', function ($scope, $localStorage, $modal, ToolsService, TaskService) {
		ToolsService.setItems([
			{
				label: 'Add Task',
				url: 'javascript:',
				onclick: function(){
					$scope.openNewTaskDialog();
				},
				icon: 'glyphicon glyphicon-plus'
			},
			{
				label: 'Delete Selected',
				url: 'javascript:',
				onclick: function(){
					$scope.deleteSelected();
				},
				icon: 'glyphicon glyphicon-remove'
			},
			{
				label: 'Prolong',
				url: 'javascript:',
				icon: 'glyphicon glyphicon-time',
			},
			{
				label: 'Task Report',
				url: 'javascript:',
				icon: 'glyphicon glyphicon-list-alt'
			}
		]);

		//init the TaskService
		TaskService.init();
		
		$scope.predicate = '-type';
		$scope.reverse = 'true';

		$scope.selected = [];
		$scope.tasks = TaskService.getTasks();
		$scope.entryTypes = TaskService.getEntryTypes();

		// $scope.$watch(function () {
	 //       return TaskService.tasks;
	 //     },                       
	 //      function(newVal, oldVal) {
	 //        $scope.tasks = newVal;
	 //    }, true);

		var updateSelected = function(action, id) {
			if (action === 'add' && $scope.selected.indexOf(id) === -1) {
				$scope.selected.push(id);
			}
			if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
				$scope.selected.splice($scope.selected.indexOf(id), 1);
			}
		};

		$scope.selectAll = function($event) {
		  var checkbox = $event.target;
		  var action = (checkbox.checked ? 'add' : 'remove');
		  for ( var i = 0; i < Object.keys($scope.tasks).length; i++) {
		    updateSelected(action, Object.keys($scope.tasks)[i]);
		  }
		};

		$scope.updateSelection = function($event, id) {
		  var checkbox = $event.target;
		  var action = (checkbox.checked ? 'add' : 'remove');
		  updateSelected(action, id);
		};

		$scope.hasSelected = function(){
			return $scope.selected.length > 0;
		};

		$scope.isSelected = function(id) {
		  return $scope.selected.indexOf(id) >= 0;
		};

		//something extra I couldn't resist adding :)
		$scope.isSelectedAll = function() {
		  return $scope.selected.length === Object.keys($scope.tasks).length;
		};

		$scope.deleteSelected = function(){
			for(var i = 0; i < $scope.selected.length; i++){
				TaskService.deleteTask($scope.selected[i]);
			}
			$scope.selected = [];
		};

		$scope.openNewTaskDialog = function(){
			var modalInstance = $modal.open({
				windowClass: 'modals in',
				templateUrl: 'views/modals/newTask.html',
				controller: 'NewTaskCtrl',
				resolve: {
					entryTypes: function () {
						return $scope.entryTypes;
					}
				}
		    });

			modalInstance.result.then(function () {

			}, function () {
				
			});	
		};

	});

app.controller('NewTaskCtrl', function($scope, $modalInstance, $localStorage, TaskService){
		$scope.wizardState = 1;
		$scope.entryTypes = TaskService.getEntryTypes();
		$scope.task = {};

		$scope.save = function (task) {

			var temp =  {
				typeId: task.type.id,
				type: task.type.name,
				validUntil : task.validUntil
			};

			var fields = task.type.fields;
			for(var i = 0; i < fields.length; i++){
				temp[fields[i].id] = fields[i].value;
			}

			TaskService.saveTask(temp);

			$modalInstance.close();	
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};

		$scope.next = function(task){
			if($scope.wizardState === 1 && task.type === undefined){
				$scope.alert = {
					type: 'danger',
					msg: 'Please select one entry type before continuing.'
				};
			}else{
				$scope.alert = undefined;
				$scope.wizardState+=1;				
			}
		};

		$scope.previous = function(){
			$scope.wizardState-=1;	
		}
	});
