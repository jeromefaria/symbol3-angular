/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
	.controller('UserListCtrl', function ($scope, ToolsService) {

		ToolsService.setItems([]);

		$scope.message = 'This is the list of Users';
		$scope.users = [];

		for (var i = 0; i < 10; i++) {
			$scope.users.push({
				name: chance.name(),
				address: chance.address(),
				street: chance.street(),
				city: chance.city(),
				phone: chance.phone(),
				birthday: chance.birthday({string: true}),
				email: chance.email()
			});
		}

	});
