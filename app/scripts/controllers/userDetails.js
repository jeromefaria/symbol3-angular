/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .controller('UserDetailsCtrl', function ($scope, $routeParams, ToolsService) {

  	ToolsService.setItems([]);

    var name = $routeParams.username;
    $scope.message = 'This is '  + name + '\'s details';
  });
