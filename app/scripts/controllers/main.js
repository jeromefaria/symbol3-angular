/* jshint devel:true */
/* global angular */
/* global jQuery */

'use strict';

angular.module('symbol3App')
	.controller('MainCtrl', function ($scope, ToolsService, $window) {
		ToolsService.setItems([]);

		/*----- variables declaration -----*/

		var menu = $('#menu');
		var header = $('#header');
		var tools = $('#tools');
		var content = $('#content');
		var search = header.find('#search');
		var menuToggle = header.find('#menuToggle');
		var searchToggle = header.find('#searchToggle');
		var toolsToggle = header.find('#toolsToggle');

		var screen = {
			size: $window.innerWidth,
			current: undefined,
			tablet: 768,
			desktop: 992,
			large: 1200
		};

		function hideSearch() {
			var s = document.getElementById('search');

			if (window.innerWidth < screen.desktop) {
				s.classList.add('hide');
			} else {
				s.classList.remove('hide');
			}
		}


		function updateWidth() {
			var content = document.getElementById('content');
			var windowWidth = window.innerWidth;

			if (windowWidth >= screen.large) {
				windowWidth -= 200;
			} else if (screen.size < screen.large && screen.size >= screen.desktop) {
				windowWidth -= 100;
			}

			content.style.width = windowWidth + 'px';
		}

		/*----- click events for header buttons -----*/

		menuToggle.on('click', function () {
			content = $('#content');
			if (menu.hasClass('menu-closed')) {
				menu
					.removeClass('menu-closed')
					.addClass('menu-opened');
				header
					.addClass('item-pushed-right');
				content
					.addClass('item-pushed-right');
			} else {
				menu
					.removeClass('menu-opened')
					.addClass('menu-closed');
				header
					.removeClass('item-pushed-right');
				content
					.removeClass('item-pushed-right');
			}
		});

		searchToggle.on('click', function (e) {
			e.preventDefault();

			var left = header.find('.left');
			var center = header.find('.center');
			var right = header.find('.right');
			var input = search.find('input[type="search"]');
			var close = search.find('span').eq(1);

			function showSearch() {
				left.hide();
				right.hide();
				center.children('.logo').addClass('hide');
				search.removeClass('hide');
				input.focus();
			}

			function hideSearch() {
				left.show();
				right.show();
				center.children('.logo').removeClass('hide');
				search.addClass('hide');
			}

			if (left.css('display') === 'block' && right.css('display') === 'block') {

				showSearch();

				close.on('click', function () {
					if (input.val() === '') {
						hideSearch();
					} else {
						console.log(input.val());
						input.val('');
					}
				});
			}
		});

		toolsToggle.on('click', function () {
			content = $('#content');
			if (tools.hasClass('tools-closed')) {
				tools
					.removeClass('tools-closed')
					.addClass('tools-opened');
				header
					.addClass('item-pushed-left');
				content
					.addClass('item-pushed-left');
			} else {
				tools
					.removeClass('tools-opened')
					.addClass('tools-closed');
				header
					.removeClass('item-pushed-left');
				content
					.removeClass('item-pushed-left');
			}
		});

		$scope.$on('$viewContentLoaded', function () {
			updateWidth();
			hideSearch();
		});

		$window.onresize = function () {
			updateWidth();
			hideSearch();
		};
	});
