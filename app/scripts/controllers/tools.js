/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .controller('ToolsCtrl', function ($scope, ToolsService) {
    
    $scope.$watch(function () {
       return ToolsService.toolsItems;
     },                       
      function(newVal, oldVal) {
        $scope.toolsItems = newVal;
    }, true);
    
    // $scope.toolsItems = [
    //   {
    //     label: 'Add',
    //     icon: 'glyphicon glyphicon-plus'
    //   },
    //   {
    //     label: 'Edit',
    //     icon: 'glyphicon glyphicon-pencil'
    //   },
    //   {
    //     label: 'Remove',
    //     icon: 'glyphicon glyphicon-remove'
    //   },
    //   {
    //     label: 'View',
    //     icon: 'glyphicon glyphicon-th'
    //   },
    // ];
  });
