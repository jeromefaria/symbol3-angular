/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .directive('s3menu', function () {
    return {
      templateUrl: 'views/directives/menu.html',
      restrict: 'E'
    };
  });