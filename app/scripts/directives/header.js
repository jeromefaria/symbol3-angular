/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .directive('s3header', function () {
    return {
    	transclude:true,
      	templateUrl: 'views/directives/header.html',
      	restrict: 'E'
    };
  });
