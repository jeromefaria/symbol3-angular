/* jshint devel:true */
/* global angular */

'use strict';

angular.module('symbol3App')
  .directive('s3tools', function () {
  	var directive = {};

  	directive.restrict = 'E';
  	directive.templateUrl = 'views/directives/tools.html';

    return directive;
  });