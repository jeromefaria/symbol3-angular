/* global angular */

'use strict';

angular.module('symbol3App', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'nvd3ChartDirectives',
  'ui.calendar',
  'ngStorage',
  'ui.bootstrap'
])
  .config(function ($routeProvider) {
    $routeProvider
      // .when('/', {
      //   templateUrl: 'views/partials/home.html',
      //   controller: 'MainCtrl'
      // })
      .when('/calendar', {
        templateUrl: 'views/partials/calendar.html',
        controller: 'CalendarCtrl'  
      })
      .when('/dashboard', {
        templateUrl: 'views/partials/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when('/charts/:setName', {
        templateUrl: 'views/partials/chart.html',
        controller: 'ChartCtrl'
      })
      .when('/users/:username', {
        templateUrl: 'views/partials/userDetails.html',
        controller: 'UserDetailsCtrl'
      })
      .when('/users', {
        templateUrl: 'views/partials/userList.html',
        controller: 'UserListCtrl'
      })
      .when('/tasks', {
        templateUrl: 'views/partials/tasks.html',
        controller: 'TasksCtrl'  
      })
      .otherwise({
        redirectTo: '/dashboard'
      });


  });
