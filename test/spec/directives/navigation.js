'use strict';

describe('Directive: navigation', function () {

  // load the directive's module
  beforeEach(module('symbol3App'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<navigation></navigation>');
    element = $compile(element)(scope);
    // expect(element.text()).toBe('this is the navigation directive');
  }));

  it('should attach a list of navigation items to the scope', function () {
    expect(scope.navigation.length).toBe(4);
  });

  it('should attach a list of navigation icons to the scope', function () {
    expect(scope.navigation.length).toBe(4);
  });

  it('should expect the visibility of the navigation to be false', function() {
    expect(scope.navigationVisibility).toBe(true);
  });
});
