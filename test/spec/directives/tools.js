'use strict';

describe('Directive: tools', function () {

  // load the directive's module
  beforeEach(module('symbol3App'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<tools></tools>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the tools directive');
  }));
});
