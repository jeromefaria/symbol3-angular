'use strict';

describe('Service: Updatewidth', function () {

  // load the service's module
  beforeEach(module('symbol3App'));

  // instantiate service
  var Updatewidth;
  beforeEach(inject(function (_Updatewidth_) {
    Updatewidth = _Updatewidth_;
  }));

  it('should do something', function () {
    expect(!!Updatewidth).toBe(true);
  });

});
