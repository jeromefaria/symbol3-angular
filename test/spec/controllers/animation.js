'use strict';

describe('Controller: AnimationCtrl', function () {

  // load the controller's module
  beforeEach(module('symbol3App'));

  var AnimationCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AnimationCtrl = $controller('AnimationCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
